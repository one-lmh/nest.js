// nest自带的管道验证 ValidationPipe
import { VersioningType, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cors from 'cors'
// import { Request, Response, NextFunction } from 'express'
// 引入普通拦截器
import { Response } from './common/response'

// 引入异常拦截器
// import { HttpFilter } from './common/filter'
// 访问静态资源
import { NestExpressApplication } from '@nestjs/platform-express'

// 中间件的使用
// const whiteList = ['/list']

// function MiddleWareAll(req: Request, res: Response, next: NextFunction) {
//   console.log(req.originalUrl);
//   if (whiteList.includes(req.originalUrl)) {
//     next()
//   } else {
//     res.send({ message: '小黑子漏出鸡脚了吧' })
//   }
//   next()
// }

import * as session from 'express-session'
import { join } from 'path';

// 全局引入守卫
// import { RoleGuard } from './guard/role/role.guard'


import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // 跨域中间件
  app.use(cors())
  // 初始化 swagger 配置项
  const options = new DocumentBuilder().addBasicAuth().setTitle('nest').setDescription('很认真').setVersion('1').build()
  // 初始化 swagger 文档
  const document = SwaggerModule.createDocument(app, options)
  // 初始化 swagger
  SwaggerModule.setup('/api-docs',app,document)
  // 可以访问静态资源配置
  app.useStaticAssets(join(__dirname, 'images'), {
    // 自定义访问路径
    prefix: '/xiaoman'
  })
  // 拦截器注入
  app.useGlobalInterceptors(new Response())
  // 异常拦截器注册
  // app.useGlobalFilters(new HttpFilter())
  // app.use(MiddleWareAll)

  // nest自带的管道验证注册
  app.useGlobalPipes(new ValidationPipe())
  // 全局使用守卫
  // app.useGlobalGuards(new RoleGuard())
  app.enableVersioning({
    type: VersioningType.URI
  })
  app.use(session({ secret: 'xiaoLi', rolling: true, name: "xiaoman.sid", cookie: { maxAge: 99999999 } }))
  await app.listen(3000);
}
bootstrap();
