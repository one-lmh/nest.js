import { Injectable } from '@nestjs/common';
import { CreateCrudDto } from './dto/create-crud.dto';
import { UpdateCrudDto } from './dto/update-crud.dto';
//进行crud
import { Repository, Like } from 'typeorm'
import { InjectRepository } from '@nestjs/typeorm'
import { Crud } from './entities/crud.entity'
import { take } from 'rxjs';
import { Tags } from './entities/tags.entity';

@Injectable()
export class CrudService {
  // crud 准备工作 依赖注入
  constructor(@InjectRepository(Crud) private readonly curd: Repository<Crud>,
    @InjectRepository(Tags) private readonly tags: Repository<Tags>) { }

  async addTags(params: { tags: string[], userId: number }) {

    const userInfo = await this.curd.findOne({ where: { id: params.userId } })

    const tagList: Tags[] = []
    for (let i = 0; i < params.tags.length; i++) {
      const T = new Tags()
      T.name = params.tags[i]
      await this.tags.save(T)
      tagList.push(T)
    }
    userInfo.tags = tagList
    this.curd.save(userInfo)
  }

  create(createCrudDto: CreateCrudDto) {
    const data = new Crud()
    // createCrudDto 前端传过来的东西
    data.name = createCrudDto.name
    data.desc = createCrudDto.desc
    // 把内容保存到数据库中
    return this.curd.save(data)
  }

  async findAll(query: { keyWord: string, page: number, pageSize: number }) {
    // 实现模糊查询
    const data = await this.curd.find({
      // 绑定关联关系
      relations: ['tags'],
      where: {
        name: Like(`%${query.keyWord}%`)
      },
      // 给id来个倒叙
      order: {
        id: 'DESC'
      },
      // 分页
      skip: (query.page - 1) * query.pageSize,
      // 展示的数量
      take: query.pageSize
    })

    const total = await this.curd.count({
      where: {
        name: Like(`%${query.keyWord}%`)
      },
    })

    return {
      data,
      total
    }
  }

  findOne(id: number) {
    return `This action returns a #${id} crud`;
  }

  update(id: number, updateCrudDto: UpdateCrudDto) {
    return this.curd.update(id, updateCrudDto)
  }

  remove(id: number) {
    return this.curd.delete(id)
  }
}
