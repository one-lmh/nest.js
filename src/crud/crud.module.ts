import { Module } from '@nestjs/common';
import { CrudService } from './crud.service';
import { CrudController } from './crud.controller';
import { Crud } from './entities/crud.entity'
import { Tags } from './entities/tags.entity'
import { TypeOrmModule } from '@nestjs/typeorm'


@Module({
  imports: [TypeOrmModule.forFeature([Crud, Tags])],
  controllers: [CrudController],
  providers: [CrudService]
})
export class CrudModule { }
