import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, Generated, ManyToMany, ManyToOne } from 'typeorm'
import {Crud} from './crud.entity'

@Entity()
export class Tags {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name:string

    // 会接收一个回调  创建关联关系的
    @ManyToOne(() => Crud)
    user:Crud
}