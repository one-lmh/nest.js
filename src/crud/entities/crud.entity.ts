import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, Generated, OneToMany } from 'typeorm'
import { Tags } from './tags.entity'

@Entity()
export class Crud {
    @PrimaryGeneratedColumn()
    id: number

    @Column({ type: 'varchar', length: 200 })
    name: string
    @Column({ type: 'varchar', length: 200 })
    desc: string
    @CreateDateColumn({ type: 'timestamp' })
    createTime: Date

    @Generated('uuid')
    uuid: string

    // 会接收一个回调  创建关联关系的,这里还需要一个回调建立反向关系
    @OneToMany(() => Tags,(tags) =>tags.user )
    tags: Tags[]

}
