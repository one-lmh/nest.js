import { ApiProperty } from '@nestjs/swagger'
export class CreateCrudDto {
    @ApiProperty({ example :'小李'})
    name:string
    @ApiProperty({ example: '最帅' })
    desc:string
}
