import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from '@nestjs/common';
import { CrudService } from './crud.service';
import { CreateCrudDto } from './dto/create-crud.dto';
import { UpdateCrudDto } from './dto/update-crud.dto';
// swagger 的配置
import { ApiBasicAuth, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger'

@Controller('crud')
@ApiTags('第一个CRUD')
export class CrudController {
  constructor(private readonly crudService: CrudService) { }

  @Post('add/tags')
  @ApiOperation({ summary: '添加tags接口', description: '给某个用户添加tags' })
  addTags(@Body() params: { tags: string[], userId: number }) {
    return this.crudService.addTags(params);
  }

  @Post()
  create(@Body() createUserDto: CreateCrudDto) {
    return this.crudService.create(createUserDto);
  }

  @Get()
  @ApiQuery({ name: 'keyWord', description: '关键词', type: String })
  @ApiQuery({ name: 'page', description: '分页', type: Number })
  @ApiQuery({ name: 'pageSize', description: '每页多少条', type: Number })
  findAll(@Query() query: { keyWord: string, page: number, pageSize: number }) {
    return this.crudService.findAll(query);
  }


  @Patch(':id')
  @ApiParam({ name: 'id', description: '用户id', required: true })
  update(@Param('id') id: string, @Body() updateUserDto: UpdateCrudDto) {
    return this.crudService.update(+id, updateUserDto);
  }

  @Delete(':id')
  @ApiParam({ name: 'id', description: '用户id', required: true })
  remove(@Param('id') id: string) {
    return this.crudService.remove(+id);
  }

}
