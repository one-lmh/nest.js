import { Controller, Get, Post, Body, Patch, Param, Delete, Inject, ParseUUIDPipe, ParseIntPipe } from '@nestjs/common';
import { ListService } from './list.service';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';

// 导出uuid里的所有文件
import * as uuid from 'uuid'

// 内置管道API
// ValidationPipe
// ParseIntPipe
// ParseFloatPipe
// ParseBoolPipe
// ParseArrayPipe
// ParseUUIDPipe
// ParseEnumPipe
// DefaultValuePipe


console.log(uuid.v4());


@Controller('list')
export class ListController {
  constructor(private readonly listService: ListService,
    @Inject('Config') private readonly base: any) { }

  @Post()
  create(@Body() createListDto: CreateListDto) {
    return this.listService.create(createListDto);
  }

  @Get()
  findAll() {
    return this.base
  }

  // @Get(':id')
  // // ParseIntPipe 传入装饰器的第二个参数就可以转换了 进行数据类型转换
  // findOne(@Param('id',ParseIntPipe) id: number) {
  //   console.log(typeof id ,'===============>');

  //   return this.listService.findOne(+id);
  // }

  @Get(':id')
  // ParseIntPipe 传入装饰器的第二个参数就可以转换了 进行数据类型转换
  findOne(@Param('id', ParseUUIDPipe) id: number) {
    console.log(typeof id, '===============>');

    return this.listService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateListDto: UpdateListDto) {
    return this.listService.update(+id, updateListDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.listService.remove(+id);
  }
}
