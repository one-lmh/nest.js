import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, SetMetadata } from '@nestjs/common';
import { GuardService } from './guard.service';
import { CreateGuardDto } from './dto/create-guard.dto';
import { UpdateGuardDto } from './dto/update-guard.dto';
// 引入守卫
import { RoleGuard } from './role/role.guard'
// 引入自定义装饰器
import { Role, ReqUrl } from './role/role.decorator'

// swagger 的配置
import { ApiBasicAuth, ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger'


@Controller('guard')
// 给接口添加权限
@ApiBasicAuth()
// 给接口添加分组
@ApiTags('守卫接口')
// 第一种用法  在Controller 加守卫 (局部使用)
@UseGuards(RoleGuard)
export class GuardController {
  constructor(private readonly guardService: GuardService) { }

  @Post()
  create(@Body() createGuardDto: CreateGuardDto) {
    return this.guardService.create(createGuardDto);
  }

  @Get()
  // 第三种 智能guard 第一个参数为key 第二个为权限
  // @SetMetadata('role',['admin'])

  // 使用自定义装饰器
  @Role('admin')
  // 给接口添加描述
  @ApiOperation({ summary: 'get接口', description: '描述XXX' })
  // 描述传的 query 参数
  @ApiQuery({ name: 'page', description: '分页信息' })
  // 返回的自定义描述
  @ApiResponse({ status: 403, description: '小黑子我是403' })
  findAll(@ReqUrl('123') url: string) {
    console.log(url, '========>url');
    return this.guardService.findAll();
  }

  @Get(':id')
  // 描述动态参数的
  @ApiParam({ name: 'id', description: '这是一个id', required: true })
  findOne(@Param('id') id: string) {
    return this.guardService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateGuardDto: UpdateGuardDto) {
    return this.guardService.update(+id, updateGuardDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.guardService.remove(+id);
  }
}
