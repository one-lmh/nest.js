import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppService2 } from './app.service2';
import { DemoController } from './demo/demo.controller';
import { DemoModule } from './demo/demo.module';
import { UserModule } from './user/user.module';
import { ListModule } from './list/list.module';
import { ConfigModule } from './config/config.module'
import { UploadModule } from './upload/upload.module';
import { LoginModule } from './login/login.module';
import { SpiderModule } from './spider/spider.module';
import { GuardModule } from './guard/guard.module';

// 引入配置数据库
import { TypeOrmModule } from '@nestjs/typeorm'
import { TestModule } from './test/test.module';
import { CrudModule } from './crud/crud.module';

@Module({
  imports: [DemoModule, UserModule, ListModule, ConfigModule.forRoot({
    path: '/xiaoli'
  }), UploadModule, LoginModule, SpiderModule, GuardModule, TypeOrmModule.forRoot({
    type: "mysql", //数据库类型
    username: "root", //账号
    password: "123456", //密码
    host: "localhost", //host
    port: 3306, //
    database: "db", //库名
    // entities: [__dirname + '/**/*.entity{.ts,.js}'], //实体文件
    synchronize: true, //synchronize字段代表是否自动将实体类同步到数据库 (生产环境不要使用这个)
    retryDelay: 500, //重试连接数据库间隔
    retryAttempts: 10,//重试连接数据库的次数
    autoLoadEntities: true, //如果为true,将自动加载实体 forFeature()方法注册的每个实体都将自动添加到配置对象的实体数组中
  }), TestModule, CrudModule],
  controllers: [AppController, DemoController],
  providers: [AppService2, {
    // providers 提供者
    provide: 'ABC',
    useClass: AppService
  }, {
      // 自定义值
      provide: 'Test',
      useValue: ['TB', 'PDD', 'JD']
    }, {
      // 工厂模式
      provide: 'CCC',
      inject: [AppService2],
      // useFactory(AppService2:AppService2){
      //   console.log(AppService2.getHello());
      //   return 123
      // }
      async useFactory(AppService2: AppService2) {
        return await new Promise((r) => {
          setTimeout(() => {
            r(AppService2.getHello())
          }, 2000)
        })
      }
    }],
})
export class AppModule { }
