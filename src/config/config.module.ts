import { Module, Global, DynamicModule } from "@nestjs/common";


interface Option {
    path: string
}

@Global()
@Module({

})

export class ConfigModule {
    static forRoot(option: Option): DynamicModule {
        return {
            module: ConfigModule,
            providers: [
                {
                    provide: "Config",
                    useValue: { baseUrl: '/api' + option.path }
                }
            ],
            exports: [
                {
                    provide: "Config",
                    useValue: { baseUrl: '/api' + option.path }
                }
            ]
        }
    }
}