import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile, Res } from '@nestjs/common';
import { UploadService } from './upload.service';
// FileInterceptor 上传单个文件  FilesInterceptor 上传多个文件
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express'

import { Response } from 'express'
import { join } from 'path'

import { zip } from "compressing";

@Controller('upload')
export class UploadController {
  constructor(private readonly uploadService: UploadService) { }

  @Post('album')
  @UseInterceptors(FileInterceptor('file'))
  upload(@UploadedFile() file) {
    console.log(file, 'file');
    return '峰峰35岁'
  }

  @Get('export')
  downLoad(@Res() res: Response) {
    // 下载文件 downLoad写法
    const url = join(__dirname, '../images/1679492188009.jpg')
    res.download(url)
  }

  @Get('stream')
  async down(@Res() res: Response) {
    const url = join(__dirname, '../images/1679492188009.png')
    const tarStream = new zip.Stream();
    await tarStream.addEntry(url)

    res.setHeader('Content-Type', 'application/octet-stream');

    res.setHeader(
      'Content-Disposition',
      `attachment; filename=xiaoli`,
    );

    tarStream.pipe(res)
  }
}
