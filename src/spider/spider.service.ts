import { Injectable } from '@nestjs/common';
import { CreateSpiderDto } from './dto/create-spider.dto';
import { UpdateSpiderDto } from './dto/update-spider.dto';
import axios from 'axios'
import * as cheerio from 'cheerio'
import * as fs from 'fs'
import * as path from 'path'

@Injectable()
export class SpiderService {


  async findAll() {
    const urls: string[] = []
    const baseUrl = 'https://www.xgmn02.com'
    const nestText = '下一页'
    let index = 0
    const getCosPlay = async () => {
      console.log(index);

      const body = await axios.get(`https://www.xgmn02.com/Cosplay/Cosplay10772${index ? '_' + index : ''}.html`)
        .then(async res => res.data)

      const $ = cheerio.load(body)

      const page = $('.pagination').eq(0).find('a')

      const pageArray = page.map(function () {
        return $(this).text()
      }).toArray()

      if (pageArray.includes(nestText)) {
        // each 先index然后item  和forEach类似
        $('.article-content p img').each(function () {
          urls.push(baseUrl + $(this).attr('src'))
        })
        index++
        await getCosPlay()
      }
      // console.log(pageArray);
      // console.log(urls);      
      // console.log($('.article-content p img').length);
    }
    await getCosPlay()
    // console.log(urls);
    this.writeFile(urls)
    return `cos`
  }

  writeFile(urls: string[]) {
    urls.forEach(async url => {
      const buffer = await axios.get(url, { responseType: 'arraybuffer' }).then(res => res.data)
      const ws = fs.createWriteStream(path.join(__dirname, '../cos' + new Date().getTime() + '.jpg'))
      ws.write(buffer)
    })
  }

}
