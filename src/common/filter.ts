import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common'

import { Request, Response } from 'express'


@Catch(HttpException)
export class HttpFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        // host的switchToHttp方法 可以获取context 里面有三个方法
        const ctx = host.switchToHttp()

        // 传个泛型 方便代码提示
        const request = ctx.getRequest<Request>()

        const response = ctx.getResponse<Response>()

        // 获取状态码
        const status = exception.getStatus()
        //status() 给前端返回的状态码 json() 给前端返回的错误信息
        response.status(status).json({
            success: false,
            // 记录错误时间
            time: new Date(),
            // 记录错误信息
            data: exception.message,
            status,
            // 显示哪个接口报的错
            path: request.url
        })
    }
}