import { Injectable, NestInterceptor, CallHandler, ExecutionContext } from '@nestjs/common'
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs'

interface Data<T> {
    data: T
}


@Injectable()  // 装饰器  提供依赖注入
// 定义一个拦截器
export class Response<T> implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<Data<T>> {
        // pipe rxjs的一个管道 方便我们处理数据
        return next.handle().pipe(map(data => {
            return {
                data,
                status: 0,
                message: '牛逼',
                success: true
            }
        }))
    }
}