import { ArgumentMetadata, HttpException, HttpStatus, Injectable, PipeTransform } from '@nestjs/common';
// plainToInstance 可以帮我们实例化CreateLoginDto
import { plainToInstance } from 'class-transformer'
// validate 验证
import { validate } from 'class-validator'

@Injectable()
export class LoginPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    // 管道里写一些验证规则
    // console.log(value,metadata);
    const DTO = plainToInstance(metadata.metatype, value)
    // 会把所有错误返回    是一个失败类型的数组
    const errors = await validate(DTO)
    if (errors.length) {
      throw new HttpException(errors, HttpStatus.BAD_REQUEST)
    }

    // console.log(DTO, '============>DTO');
    console.log(errors, '==========>errors');


    return value;
  }
}
