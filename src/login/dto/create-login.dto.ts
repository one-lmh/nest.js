// 引入的都是一些装饰器
import { IsNotEmpty, IsString, Length ,IsNumber} from 'class-validator'

export class CreateLoginDto {
    @IsNotEmpty()
    @IsString()
    @Length(5, 10, {
        message: '字符长度5-10'
    })
    name: string
    @IsNumber()
    age: number
}
